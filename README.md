These are the inputs for NExIGO to compile a negative dataset for VFranger (to be published).

***
VFs_lists.tar.gz contains a list of VFs and a list of VF-likes (Accessions).
Each Accession is assigned to a annotation file (not required for NExIGO). 
***
 AFs.tar.gz contains 254 AF from 254 Uniparc Proteoms, which are the minimum set cover for the VFs list. 
 ***
 
 Results.tar.gz:  
 Results of NExIGO using these lists and annotation files
 with these Parameters (if not NULL or Default):
 + suffix = ".argot2"
 + threshold_TS = 200, 
 + ignoreTheseGOIDs = c("GO:0005488")
 + paths referring to directories containing the according files
  
  